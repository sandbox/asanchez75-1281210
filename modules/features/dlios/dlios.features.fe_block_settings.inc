<?php

/**
 * Implementation of hook_default_fe_block_settings().
 */
function dlios_default_fe_block_settings() {
  $export = array();

  // garland
  $theme = array();

  $theme['user-1'] = array(
    'module' => 'user',
    'delta' => '1',
    'theme' => 'garland',
    'status' => '1',
    'weight' => '0',
    'region' => 'left',
    'custom' => '0',
    'throttle' => '0',
    'visibility' => '0',
    'pages' => '',
    'title' => '',
    'cache' => '-1',
  );

  $export['garland'] = $theme;

  $theme_default = variable_get('theme_default', 'garland');
  $themes = list_themes();
  foreach ($export as $theme_key => $settings) {
    if ($theme_key != $theme_default && empty($themes[$theme_key]->status)) {
      unset($export[$theme_key]);
    }
  }
  return $export;
}
