<?php

/**
 * Implementation of hook_node_info().
 */
function dlios_node_info() {
  $items = array(
    'dlios' => array(
      'name' => t('Document'),
      'module' => 'features',
      'description' => t('This content type is for Document-Like-Information-Objects: generically, documents and resources that have a title, an author and a topic.
If you manage a repository, all resources in your repository will be of this type.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'has_body' => '0',
      'body_label' => '',
      'min_word_count' => '0',
      'help' => '',
    ),
  );
  return $items;
}

/**
 * Implementation of hook_views_api().
 */
function dlios_views_api() {
  return array(
    'api' => '2',
  );
}
