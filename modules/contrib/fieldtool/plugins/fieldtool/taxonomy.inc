<?php
// $Id: taxonomy.inc,v 1.2 2010/02/14 02:16:12 mjd Exp $
/**
 * @file
 * Field Tool - Taxonomy plugin
 * Taxonomy field handler for Field Tool module.
 *
 * Copyright 2009 Matthew Davidson.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Implementation of [module]_[plugin]_fieldtool_fields().
 */
function fieldtool_taxonomy_fieldtool_fields() {
  if (!module_exists('taxonomy')) {
    return array();
  }
  
  $node_fields = array();

  $node_fields['taxonomy']['label'] = t('Taxonomy');
  $node_fields['taxonomy']['stored'] = TRUE;
  $node_fields['taxonomy']['getter callback'] = 'fieldtool_field_taxonomy_get';
  $node_fields['taxonomy']['setter callback'] = 'fieldtool_field_taxonomy_set';
  $node_fields['taxonomy']['export callback'] = 'fieldtool_field_taxonomy_export';
  $node_fields['taxonomy']['import callback'] = 'fieldtool_field_taxonomy_import';
  $node_fields['taxonomy']['fields'] = array();

  $node_types = array();

  foreach (taxonomy_get_vocabularies() as $vid => $vocabulary) {
    $node_fields['taxonomy_'. $vid]['label'] = $vocabulary->name;
    $node_fields['taxonomy_'. $vid]['stored'] = TRUE;
    $node_fields['taxonomy_'. $vid]['virtual'] = TRUE;
    $node_fields['taxonomy_'. $vid]['taxonomy vid'] = $vid;
    $node_fields['taxonomy_'. $vid]['weight'] = $vocabulary->weight;
    $node_fields['taxonomy_'. $vid]['getter callback'] = 'fieldtool_field_taxonomy_get';
    $node_fields['taxonomy_'. $vid]['setter callback'] = 'fieldtool_field_taxonomy_set';
    $node_fields['taxonomy_'. $vid]['export callback'] = 'fieldtool_field_taxonomy_export';
    $node_fields['taxonomy_'. $vid]['import callback'] = 'fieldtool_field_taxonomy_import';
    $node_fields['taxonomy']['fields'][] = 'taxonomy_'. $vid;
    
    foreach ($vocabulary->nodes as $node_type) {
      $node_fields['taxonomy']['node types'][$node_type] = $node_type;
      $node_fields['taxonomy_'. $vid]['node types'][] = $node_type;
    }
  }
  
  $node_fields['taxonomy']['node types'] = array_keys($node_fields['taxonomy']['node types']);

  return array('node' => $node_fields);
}

/**
 * Getter callback.
 *
 * @return
 *   An array like array($tid => $tid).
 */
function fieldtool_field_taxonomy_get($node, $field_name = 'taxonomy', $field_info) {
  $values = array();

  foreach (array_keys($node->taxonomy) as $tid) {
    if ($field_info['taxonomy vid']) { // If this is a vocabulary field
      $term = taxonomy_get_term($tid);
      if ($term->vid == $field_info['taxonomy vid']) {
        $values[$tid] = $tid;
      }
    }
    else {
      $values[$tid] = $tid;
    }
  }
  return $values;
}

/**
 * Setter callback.
 *
 * @param $value
 *   If you want to delete a value the array key must be the tid, and the 
 *   value evaluate to FALSE in boolean context. Otherwise we just ignore the
 *   key and use the value as the tid. To delete all values, pass FALSE rather
 *   than an array.
 */

function fieldtool_field_taxonomy_set(&$node, $field_name = 'taxonomy', $value, $field_info) {
  if ($value === FALSE) { // We want to delete all terms
    if ($field_info['taxonomy vid']) {
      foreach ($node->taxonomy as $tid => $term) {
        if ($term->vid == $field_info['taxonomy vid']) {
          unset($node->taxonomy[$tid]);
        }
      }
    }
    else {
      $node->taxonomy = array();
    }
    return;
  }
  
  foreach ($value as $key => $tid) {
    if ($field_info['taxonomy vid']) { 
      // If this is a vocabulary field we do have to do some validation.
      $term = $tid ? taxonomy_get_term($tid) : taxonomy_get_term($key); // In case we're deleting a term
      if ($term->vid != $field_info['taxonomy vid']) {
        continue;
      }
    }
    
    if (!$tid && array_key_exists($key, $node->taxonomy)) {
      unset($node->taxonomy[$key]);
    }
    elseif (is_numeric($tid)) {
      $node->taxonomy[$tid] = taxonomy_get_term(fieldtool_taxonomy_value_itemize($field_info, $tid));
    }
  }
}

/**
 * Taxonomy value itemization callback.
 *
 * Can accept practically anything, as this is used for imported values.
 *
 * @param $type
 *  Optionally specify content type to ensure returned terms are in relevant vocabularies.
 *
 * @return
 *   A term id.
 */
function fieldtool_taxonomy_value_itemize($field_info, $value, $type = NULL) {
  // Catch term as $tid (eg. from fieldtool_field_taxonomy_get()) or as term object/array
  if (is_array($value) && $value['tid'] && taxonomy_get_term($value['tid'])) {
    $tid = $value['tid'];
  }
  elseif (is_object($value && $value->tid && taxonomy_get_term($value->tid))) {
    $tid = $value->tid;
  }
  elseif (is_numeric($value) && taxonomy_get_term($value)) {
    $tid = $value;
  }

  // filter by vocabularies for this content type
  if ($type) {
    $term = taxonomy_get_term($tid);
    if (!in_array($term->vid, array_keys(taxonomy_get_vocabularies($type)))) {
      unset($tid);
    }
  }
  
  //TODO: Maybe doing this sloppy search should be optionally triggered by an argument.
  if (!$tid) {
    $tid = fieldtool_taxonomy_term_best_match($value, $node->type);
  }
  
  return $tid;
}

function fieldtool_field_taxonomy_export($node, $field_name, $field_info) {
  $terms = $node->taxonomy;
  if ($field_info['taxonomy vid']) { // If this is a vocabulary field
    foreach ($terms as $tid => $term) {
      if ($term->vid != $field_info['taxonomy vid']) {
        unset($terms[$tid]);
      }
    }
  }

  return $terms;
}

/**
 * Work out whether the imported value is a single item or array of items
 */
function fieldtool_field_taxonomy_import(&$node, $field_name = 'taxonomy', $value, $field_info) {
  if ($value === FALSE) { // We want to delete all terms
    fieldtool_field_taxonomy_set($node, $field_name, $value, $field_info);
    return;
  }
  
  /* TODO: Can't currently do the $value = array($tid => 0) trick from 
     fieldtool_field_taxonomy_set() here. Not clear that this is desirable,
     as importers won't necessarily know which terms are set without using 
     the getter callback anyway. */
  
  $terms = array();
  if (is_array($value)) {
    foreach ($value as $term) {
      if ($tid = fieldtool_taxonomy_value_itemize($field_info, $term, $node->type)) {
        $terms[] = $tid;
      }
    }
  }
  elseif (is_numeric($value)) {
    $terms = array(fieldtool_taxonomy_value_itemize($field_info, $value, $node->type));
  }
  elseif (is_string($value) && ($tid = fieldtool_taxonomy_term_best_match($value, $node->type))) {
    $terms = array($tid);
  }
  fieldtool_field_taxonomy_set($node, $field_name, $terms, $field_info);
}

/**
 * Search for a term that corresponds to a string, assuming we're importing from non-Drupal source.
 *
 * @param $value
 *  A string which may match a taxonomy term name or synonym. 
 *  WARNING: If numeric, will NOT look for matching tid. Use taxonomy_get_term() to check that first if necessary. 
 *
 * @param $type
 *  Optionally specify content type to ensure returned terms are in relevant vocabularies.
 *
 * @return
 *  A term id.
 */
function fieldtool_taxonomy_term_best_match($value, $type = NULL) {
  $matches = taxonomy_get_term_by_name($value);

  if ($type) { // filter by vocabularies for this content type
    $vids = array_keys(taxonomy_get_vocabularies($type));
    foreach ($matches as $term) {
      if (in_array($term->vid, $vids)) {
        $terms[] = $term;
      }
    }
  }
  else {
    $terms = $matches;
  }
  
  if (count($terms)) { //TODO: What to do if there's more than one term?
    return $terms[0]->tid;
  }
  
  // Can't find matching term name, look for synonyms
  if ($term = taxonomy_get_synonym_root($value)) {
    if (count($vids) && in_array($term->vid, $vids)) {
        return $term->tid;
    }
    else {
        return $term->tid;
    }
  }
}




