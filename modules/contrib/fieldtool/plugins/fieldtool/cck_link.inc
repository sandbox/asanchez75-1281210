<?php
// $Id: cck_link.inc,v 1.3 2010/02/16 00:20:44 mjd Exp $
/**
 * @file
 * Field Tool -
 * An API for programmatically accessing fields of objects in a generic way.
 *
 * Copyright 2009 by Jakob Petsovits ("jpetso", http://drupal.org/user/56020)
 */

/**
 * Implementation of [module]_[plugin]_fieldtool_cck_field_types().
 */
function fieldtool_cck_link_fieldtool_cck_field_types() {
  $types = array();

  $types['link'] = array(
    'cck extraction callback' => 'fieldtool_cck_link_value_extract',
    'cck itemization callback' => 'fieldtool_cck_link_value_itemize',
    'non-array value' => TRUE,
    'multiple' => TRUE,
    'transformations slot property callback' => 'fieldtool_cck_link_transformations_slot_property',
    'cck subfields' => array('url','title','attributes'),
  );
  
  return array('cck field type' => $types);
}

/**
 * CCK value extraction callback.
 */
function fieldtool_cck_link_value_extract($field_info, $item) {
  return $item;
}

/**
 * CCK value itemization callback.
 */
function fieldtool_cck_link_value_itemize($field_info, $value) {
  if (is_array($value)) {
    return $value;
  }
  elseif (is_string($value)) {
    return array('url' => $value);
  }
}

/**
 * Transformations slot property callback.
 */
function fieldtool_cck_link_transformations_slot_property($field_info, $property_key) {
  switch ($property_key) {
    case 'expectedType':
      return 'php:type:string';
  }
}
