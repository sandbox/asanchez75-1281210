<?php
// $Id: cck_nodereference.inc,v 1.2 2009/07/15 13:01:03 jpetso Exp $
/**
 * @file
 * Field Tool -
 * An API for programmatically accessing fields of objects in a generic way.
 *
 * Copyright 2009 by Jakob Petsovits ("jpetso", http://drupal.org/user/56020)
 */

/**
 * Implementation of [module]_[plugin]_fieldtool_cck_field_types().
 */
function fieldtool_cck_nodereference_fieldtool_cck_field_types() {
  $types = array();

  $types['nodereference'] = array(
    'cck extraction callback' => 'fieldtool_cck_nodereference_value_extract',
    'cck itemization callback' => 'fieldtool_cck_nodereference_value_itemize',
    'non-array value' => TRUE,
    'transformations slot property callback' => 'fieldtool_cck_nodereference_transformations_slot_property',
  );
  return array('cck field type' => $types);
}

/**
 * CCK value extraction callback.
 */
function fieldtool_cck_nodereference_value_extract($field_info, $item) {
  return $item['nid'];
}

/**
 * CCK value itemization callback.
 */
function fieldtool_cck_nodereference_value_itemize($field_info, $value) {
  return array('nid' => $value);
}

/**
 * Transformations slot property callback.
 */
function fieldtool_cck_nodereference_transformations_slot_property($field_info, $property_key) {
  switch ($property_key) {
    case 'expectedType':
      return 'php:type:integer';
  }
}
