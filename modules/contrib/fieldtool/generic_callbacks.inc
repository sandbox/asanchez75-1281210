<?php
// $Id: generic_callbacks.inc,v 1.1 2010/02/14 02:16:12 mjd Exp $
/**
 * @file
 * Field Tool - Generic callbacks
 *
 * Generic field handler callbacks for Field Tool.
 *
 * Copyright 2009 Matthew Davidson (http://drupal.org/user/52996).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


/**
 * Generic fallback getter callback. Fields with no explicitly declared
 * getter callback and no child fields are assumed to want this as their
 * getter callback.
 */
function fieldtool_field_generic_get($entity, $field_name, $field_info, $deltas = array()) {
  $path = _fieldtool_delta_replace($field_info['field path'], $deltas);

  if (is_array($entity)) {
    $path = _fieldtool_object_path_to_array($path);
    return eval('return isset($entity'. $path .') ? $entity'. $path .' : NULL;');
  }
  return eval('return isset($entity->'. $path .') ? $entity->'. $path .' : NULL;');
}

/**
 * Generic fallback setter callback. Fields with no explicitly declared
 * setter callback and no child fields are assumed to want this as their
 * setter callback.
 */
function fieldtool_field_generic_set(&$entity, $field_name, $value, $field_info, $deltas = array()) {
  $path = _fieldtool_delta_replace($field_info['field path'], $deltas);
  if (is_array($entity)) {
    $path = _fieldtool_object_path_to_array($path);
    if (isset($value)) {
      eval('$entity'. $path .' = $value;');
    }
    else {
      eval('unset($entity'. $path .');');
    }
  }
  else {
    if (isset($value)) {
      eval('$entity->'. $path .' = $value;');
    }
    else {
      eval('unset($entity->'. $path .');');
    }
  }
}

/**
 * Generic fallback import callback. Fields with no explicitly declared
 * import callback and no child fields are assumed to want this as their
 * import callback.
 */
function fieldtool_field_generic_import(&$entity, $field_name, $value, $field_info, $deltas = array()) {
  return fieldtool_set($field_info, $entity, $value, $deltas);
}

/**
 * Generic fallback export callback. Fields with no explicitly declared
 * export callback and no child fields are assumed to want this as their
 * export callback.
 */
function fieldtool_field_generic_export($entity, $field_name, $field_info, $deltas = array()) {
  return fieldtool_get($field_info, $entity, $deltas);
}

/**
 * Generic fallback extraction callback. Fields with no explicitly declared
 * extraction callback and no child fields are assumed to want this as their
 * extraction callback.
 */
function fieldtool_generic_value_extract($field_info, $item) {
  return $item;
}

/**
 * Generic fallback itemization callback. Fields with no explicitly declared
 * itemization callback and no child fields are assumed to want this as their
 * itemization callback.
 * 
 * We assume that the setter callback wants a scalar value and try to
 * convert objects and arrays in the most sensible way possible. Relying on
 * this is often a BAD IDEA. If you have a last-descendant or "leaf" field
 * that requires a non-scalar value for the setter callback, or can reasonably 
 * expect non-scalar import data, please write your own itemization callback.
 */
function fieldtool_generic_value_itemize($field_info, $value) {
  if (is_object($value)) {
    $value = (array) $value;
  }
  
  if (is_array($value)) {
    if (_fieldtool_is_keyed_array($value)) {
      $temp = array();
      // This will not work satisfactorily with multidimensional arrays.
      // Did I mention this is risky?
      foreach ($value as $key => $val) {
        $temp[] = $key .': '. $val;
      }
      $value = $temp;
    }
    
    $value = implode(', ', $value);
  } 
  
  return $value;
}

/**
 * Helper function for generic callbacks.
 */
function _fieldtool_delta_replace($path, $deltas) {
  foreach ($deltas as $delta) {
    $pos = strpos($path, '[%]');
    if ($pos === FALSE) {
      // Assume we're trying to get an item on a field with a path that
      // doesn't explicitly declare the tail '[%]'
      $path .= "[$delta]";
    }
    else {
      $path = substr_replace($path, (int) $delta, $pos + 1, 1);
    }
  }
  
  // Assume that if there's a placeholder remaining on the end,
  // we want all values.
  $path = preg_replace('/\[\%\]$/', '', $path);
  
  // Assume for any leftover placeholders we wanted the first item
  $path = str_replace('[%]', '[0]', $path);
  
  return $path;
}

/**
 * Helper function for generic callbacks.
 *
 * Good gracious, this is kludgy.
 */
function _fieldtool_object_path_to_array($path) {
  $temp = explode('->', $path);
  $temp = explode('[', $temp[0]);
  
  return preg_replace('/^'. $temp[0] .'/', "['". $temp[0] ."']", $path);
}

/**
 * TODO: Recursive getter/setter/import/export/itemize/extract callbacks
 *
 * These are fallbacks for fields _with_ child fields, but no defined callback.
 */


/**
 * Assign generic callback to a field if a callback isn't defined.
 */
function _fieldtool_find_callbacks(&$field_info) {
  $callbacks = array(
    'getter' => 'fieldtool_field_generic_get',
    'setter' => 'fieldtool_field_generic_set',
    'import' => 'fieldtool_field_generic_import',
    'export' => 'fieldtool_field_generic_export',
    'extraction' => 'fieldtool_field_generic_value_extract',
    'itemization' => 'fieldtool_field_generic_value_itemize',
  );
  
  foreach ($callbacks as $type => $function) {
    $field_info[$type .' callback'] = $field_info[$type .' callback'] ? $field_info[$type .' callback'] : $callbacks[$type];
  }
    
  return $field_info;
}


/**
 * Work out path back to root field for generic getter/setter callbacks.
 *
 * @param $entity_type
 *   The thing that you want to retrieve field information about, e.g. 'node'
 *   for node types.
 * @param $field_info
 *   One of the field information arrays returned by fieldtool_fields().
 *
 * @return
 *   An string representing the path to the (sub)field from the root field,
 *   in the usual PHP variable notation, so you getter/setter callbacks can
 *   access the value via eval('return $object->'.$path).
 */
function fieldtool_path($entity_type, $field_info) {
  $path = array();
  while($parent = fieldtool_get_parent($entity_type, $field_info)) {
    if (!$field_info['virtual']){
      // Strip any helpful '_%_' delta placeholders in names
      $parent['name'] = str_replace('_%_', '_', $parent['name']);
      $field_info['name'] = str_replace('_%_', '_', $field_info['name']);
      $type = is_array($parent['fields']) ? 'array' : 'object';
      $bit = array(
        'type' => $type,
        'name' => preg_replace('/^'. $parent['name'] .'_/', '', $field_info['name']),
        'multiple' => $field_info['multiple'],
      );
      array_unshift($path, $bit);
    }
    
    $field_info = $parent;
  }

  // Root field. Last parent found that did not have a parent.
  if (!$field_info['virtual']) {
      $bit = array(
        'type' => 'root',
        'name' => $field_info['name'],
        'multiple' => $field_info['multiple'],
      );
      array_unshift($path, $bit);
  }
  
  $string = '';
  foreach ($path as $i => $bit) {
    if (($bit['type'] == 'array') && ($i > 0)) {
      $string .= "['". $bit['name'] ."']". _fieldtool_multiple_placeholder($bit['multiple']);
    }
    elseif (($bit['type'] == 'object') && ($i > 0)) {
      $string .= '->'. $bit['name']. _fieldtool_multiple_placeholder($bit['multiple']);
    }
    else {
      $string .= $bit['name']. _fieldtool_multiple_placeholder($bit['multiple']);
    }
  }
  
  return $string;
}

/**
 * Helper function for fieldtool_path().
 */
function _fieldtool_multiple_placeholder($multiple = FALSE) {
  if ($multiple) {
    return '[%]';
  }
  return '';
}


