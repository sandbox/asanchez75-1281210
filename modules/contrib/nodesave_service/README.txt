// $Id: README.txt,v 1.1.2.1 2009/10/13 20:02:42 dixon Exp $

Node Save Service
=================

Description
-----------

This module provides a new service method named nodesave.save that uses
node_save() instead of drupal_execute().

Maintainers
-----------

Dick Olsson

Sponsors
--------

NodeOne
