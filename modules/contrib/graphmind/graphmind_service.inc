<?php
// $Id: graphmind_service.inc,v 1.1.2.1 2009/11/28 10:18:50 itarato Exp $


/**
 * Returns all the available views for GraphMind.
 * @return array
 */
function graphmind_service_get_views() {
  $data = array();

  $tags = variable_get('graphmind_views_tags', '');
  $tags = str_replace(' ', '', $tags);
  $tag_array = explode(',', $tags);

  $views = views_get_all_views();
  foreach ((array)$views as $view) {

    $views_tags = str_replace(' ', '', $view->tag);
    $views_tag_array = explode(',', $views_tags);
    $tag_is_match = strlen($tags) == 0;

    foreach ((array)$views_tag_array as $view_tag) {
      if (in_array($view_tag, $tag_array)) {
        $tag_is_match = TRUE;
      }
    }

    if (!$tag_is_match) continue;

    $data[] = array(
      'name' => $view->name,
      'baseTable' => $view->base_table,
      'tag' => $view->tag,
      'disabled' => $view->disabled,
    );
  }

  return $data;
}


/**
 * Save a GraphMind map. (FreeMind XML format.)
 * @param int $nid
 * @param string $mm
 * @return string
 */
function graphmind_service_save_graphmind($nid, $mm, $change_time) {
  $node = node_load($nid);
  $node->body = $mm;

  // @TODO multiedit interruption
  if (node_last_changed($node->nid) > $change_time) {
    return t('This content has been modified by another user, changes cannot be saved.');
  }

  node_save($node);
  return '1';
}
