INSTRUCTIONS

1. Download agrovocfield module and its dependences (cck + agrovoc module + i18n) and store in your sites/all/modules 
2. Enable module agrovocfield_autocomplete to enable its dependences automatically.
3. Go to admin/settings/language and add your languages. I recommend you use l10n_update module to get and synchronize languages from localize.drupal.org
4. Go to admin/settings/language/configure and select 'Path prefix only' option
5. Go to admin/build/block and enable 'Language switcher' block in some region of your site.
6. Go to admin/content/taxonomy to create a taxonomy for fetching and storing terms from Agrovoc Server and enable option 
   > Multilingual options
    > Translation mode
     > Per language terms. Different terms will be allowed for each language and they can be translated. [x]
   Is not necessary select some kind of content type. You can leave 'Content types' section empty.
7. If you want to try with a content type, you can use story content type and go to admin/content/node-type/story and enable the following option
  
   > Workflow settings
     > Multilingual support:
      > Enabled, with translation [x]
  
  And save it. Open again story content settings and enable the following option:
 
   > Multilanguage options:
     > Options for node language
       > Require language          [x]
      
8. Create a agrovocfield field for story content type (as example for this demo).  In 'Global settings' section select as Vocabulary the name
   of taxonomy you just created and select option 'Number of values:' as 'Unlimited' . This is very important to fetch multiples values. 
   Next, save settings.
9. Create a story node and you will find a text box for fetching data from Agrovoc Server. If you type some characters according the word
   you are searching, you should see a list of suggestions. Select one and continue, after, you can save the node. 
   Now, if you switch language of your website, you will see how this module translates the terms you have selected automatically. :)
  
  Enjoy!
    
Notes.

- This module works well with multilanguage installation from scratch. 
  At the beginning, is very important that you select languages that you will use in your website.
- The Agrovoc Field module is a module for manually indexing nodes using Agrovoc concepts.
  This module creates a new CCK field (called Agrovoc and derived from the Content Taxonomy field) that 
  allows to select Agrovoc terms through an autocomplete textbox connecting to the Agrovoc web services 2.0.
  Being a CCK field synchronized with a taxonomy, it allows to exploit both CCK and taxonomy features in Drupal.
  The module has very good and very well integrated multi-lingual support: when indexing a node, the module lists 
  Agrovoc terms in the website currently active language, but then stores the selected terms in all the languages 
  enabled in the website. When switching to a different language, the corresponding Agrovoc terms in that language 
  will be displayed and when translating a node that has already been indexed, the translated Agrovoc terms will be 
  displayed and the autocomplete field will only list terms in that language.
  The author of this module is already in contact with the author of another Agrovoc Drupal module 
  (http://drupal.org/project/agrovoc) to coordinate on development.
  Both modules derive from the same initial developments in CONDESAN and use the same basic functionalities (a common API), 
  but the Agrovoc module only integrates Agrovoc with the Drupal taxonomy, while the Agrovoc Field module integates it also with CCK.
  Coordination on the development of these two modules will probably lead to the release 
  of a co-maintained module called "Agrovoc API" and two different interface modules. 
  The next version of the module (or the next version of the Agrovoc API module) will include the option
  to use the Agrovoc SPARQL endpoint instead of the soon obsolete Agrovoc web services 2.0. 
  It was developed by CONDESAN in coordination with the AIMS team in FAO and with the IT team in GFAR.
