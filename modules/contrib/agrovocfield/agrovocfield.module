<?php
/**
 * @file
 *  Agrovoc CCK field
 *  This module provides an CCK field to fetch and store terms from AGROVOC thesaurus
 *  Some portions of code were taken from Content Taxonomy Module and adapted to our objectives.
 */

/*
 * Agrovoc Server URI
 */

define('AGROVOC_URI', 'http://aims.fao.org/aos/agrovoc/c_');

/**
 * Implementation of hook_field_info().
 */
function agrovocfield_field_info() {
  return array(
    'agrovocfield' => array(
      'label' => 'Agrovoc field',
        'description' => 'CCK field for AGROVOC',
        'callbacks' => array(
          'tables' => CONTENT_CALLBACK_DEFAULT,
          'arguments' => CONTENT_CALLBACK_DEFAULT,  
        ),
      ),
    );
}

/**
 * Implementation of hook_field().
 */
function agrovocfield_field($op, &$node, $field, &$items, $teaser, $page) {
  switch ($op) {
    case 'presave':
       global $language;
          foreach ($items as $item) {
            if ($item['value']) {
            $obj = agrovocfield_get_tids_from_labels_object(unserialize($item['value']), $field['#vid']);
            $tid = $obj->labels[$language->language]['tid'];
            if (is_array($node->taxonomy[$field['vid']])) {
              if (!in_array($tid, $node->taxonomy[$field['vid']])) {
                $node->taxonomy[$field['vid']][] = $tid;
              }
            }
            // when saving an existing node without presenting a form to the user,
            // the terms are objects keyed by tid. there's no need to re-set these
            // terms, and to do so causes php warnings because the database rejects
            // the row insert because of primary key constraints.
            else {
              $node->taxonomy[$field['vid']] = array($tid);
              }
            }
         }
         if (empty($node->taxonomy)) {
          $node->taxonomy[$field['vid']] = NULL;
        }
      
      break;
  }
}

/**
 * Implementation of hook_field_settings().
 */
function agrovocfield_field_settings($op, $field) {
  switch ($op) {
      case 'form': 
        $options_voc = array();
      foreach (taxonomy_get_vocabularies() as $voc) {
        $options_voc[$voc->vid] = $voc->name;
      }
      $form['vid'] = array(
        '#title' => t('Vocabulary'),
        '#type' => 'select',
        '#default_value' => is_numeric($field['vid']) ? $field['vid'] : 0,
        '#options' => $options_voc,
        '#description' => t('Terms of the selected vocabulary get exposed to the field'),
      );
      return $form;
    case 'save':
      return array('vid'); 
    case 'database columns':
      return array(
          'value' => array('type' => 'text', 'size' => 'medium', 'not null' => FALSE),        
      );
  }
}  

/**
 * Implementation of hook_theme().
 */
function agrovocfield_theme() {
  return array(
    'agrovocfield_formatter_default' => array(
      'arguments' => array('element' => NULL),
    ),
    'agrovocfield_formatter_link' => array(
      'arguments' => array('element' => NULL),
    ),
    'agrovocfield_formatter_uri' => array(
      'arguments' => array('element' => NULL),
    ),    
  ); 
}

/**
 * Implementation of hook_field_formatter_info().
 */
function agrovocfield_field_formatter_info() {
  return array(
    'default' => array(
      'label' => t('As Text'),
      'field types' => array('agrovocfield'),
      'multiple values' => CONTENT_HANDLE_CORE,
    ),
    'link' => array(
      'label' => t('As Link'),
      'field types' => array('agrovocfield'),
      'multiple values' => CONTENT_HANDLE_CORE,
    ),
    'uri' => array(
      'label' => t('As URI'),
      'field types' => array('agrovocfield'),
      'multiple values' => CONTENT_HANDLE_CORE,
    ),    
  );
}


/**
 * Theme function for 'default' text field formatter.
 */
function theme_agrovocfield_formatter_default($element) {
    if (!empty($element['#item']['value'])) {
      global $language;
      $obj = unserialize($element['#item']['value']);
      return check_plain(agrovocfield_set_label_lang($obj, $language->language));
    }
}

/**
 * Theme function for 'link' text field formatter.
 */
function theme_agrovocfield_formatter_link($element) {
  if (!empty($element['#item']['value'])) {
    global $language;
    $field = content_fields($element['#field_name'], $element['#type_name']);
    $term_object = unserialize($element['#item']['value']);
    $term = agrovocfield_get_tids_from_labels_object($term_object , $field['vid']);
    $label = $term->labels[$language->language]['name'];
    $tid = $term->labels[$language->language]['tid'];
    return l($label, 'taxonomy/term/' . $tid, array('attributes' => array('rel' => 'tag', 'title' => $label)));
  }
}

/**
 * Theme function for 'uri' text field formatter.
 */
function theme_agrovocfield_formatter_uri($element) {
  if (!empty($element['#item']['value'])) {
    global $language;
    $field = content_fields($element['#field_name'], $element['#type_name']);
    $term_object = unserialize($element['#item']['value']);
    $term = agrovocfield_get_tids_from_labels_object($term_object , $field['vid']);
    $label = $term->labels[$language->language]['name'];
    $agrovoc_id     = $term->id;
    return l($label, AGROVOC_URI . $agrovoc_id, array('attributes' => array('rel' => 'tag', 'title' => $label)));
  }
}

/**
 * Implementation of hook_content_is_empty().
 */
function agrovocfield_content_is_empty($item, $field) {
  if (empty($item['value'])) {
    return TRUE;
  }
  return FALSE;
}

/**
 * Adapted from taxonomy_get_term_by_name function.  
 * I did it because I can't use hook_db_rewrite_sql to avoid filters by languages
 *  
 * @param string $name
 *   Term searched
 * @return 
 *   array of objects for each term found
 */
function agrovocfield_get_term_by_name($name) {
  $db_result = db_query("SELECT t.* FROM {term_data} t WHERE LOWER(t.name) = LOWER('%s')", trim($name));
  $result = array();
  while ($term = db_fetch_object($db_result)) {
    $result[] = $term;
  }
  return $result;
}

/**
 * Adapted from taxonomy_get_term_by_name function.  
 * I did it because I need a special query using vocabulary id
 * 
 * @param string $name
 *   term searched
 * @param integer $vid
 *   vocabulary id
 * @return 
 *   array of objects for each term found 
 */
function agrovocfield_get_term_by_name_and_by_vid($name, $vid) {
  //$db_result = db_query("SELECT t.tid, t.* FROM {term_data} t WHERE LOWER(t.name) = LOWER('%s') AND t.vid = '%d' LIMIT 0, 1", trim($name), $vid);
  $args = array(trim($name), $vid);
  $db_result = db_query_range("SELECT t.tid, t.* FROM {term_data} t WHERE LOWER(t.name) = LOWER('%s') AND t.vid = '%d'", $args, 0, 1);
  $result = array();
  while ($term = db_fetch_object($db_result)) {
    $result[] = $term;
  }
  return $result;
}

/**
 * Load a Agrovoc Terms Object 
 * 
 * @param string $label
 *   Agrovoc term 
 * @return 
 *   object containing others languages and agrovoc id that belongs to this label
 */
function agrovocfield_load_object($label) {
  $term = agrovoc_api_simple_search_by_mode2($label, 'exact', ',', TRUE, 10);
  $term_labels = agrovoc_api_get_all_labels_by_termcode2($term[0]['id']);
  $languages_supported =  i18n_supported_languages();
  $obj = new stdClass();
  $obj->id = $term[0]['id'];
  foreach ($languages_supported  as $k => $v) {
   // Use substr because FAO Chinese language is ZH and Drupal Chinese language is zh-hans, 
   // therefore, you  need only 2 first characters.
  $labels[$k] = $term_labels[strtoupper(substr($k, 0, 2))];
  }
  $obj->labels = $labels;
  return $obj;
}

/**
 * Return a label of Agrovoc Terms Object
 * 
 * @param object $obj 
 *   Agrovoc Terms Object
 * @param string $lang
 *   language, use 2 characters 
 * @return 
 *   string label in required language
 */
function agrovocfield_set_label_lang($obj, $lang) {
  return $obj->labels[$lang] ; 
}

/**
 * Get translation nid value for a given value of nid
 * 
 * @param integer $nid
 *   node id
 * @return 
 *   tnid or FALSE 
 */
function agrovocfield_get_tnid($nid) {
  $sql = "SELECT tnid FROM {node} WHERE nid = '%d'";
  $tnid = db_result(db_query($sql, $nid));
  return $tnid;
}

/**
 * Add terms id to Agrovoc Terms Object
 * 
 * @param object $obj
 *   Agrovoc Terms Object
 * @param integer $vid
 *   vocabulary id
 * @return 
 *   object modified containing tids stored for each label 
 */
function agrovocfield_get_tids_from_labels_object(&$obj, $vid) {
  $terms = $obj->labels;
  // Reseting container.
  unset($obj->labels);
  foreach ($terms as $k => $v) {
    $stored_terms = agrovocfield_get_term_by_name($v);
     foreach ($stored_terms as $stored_term) {
            if (($vid = $stored_term->vid) && ($stored_term->language == $k)) {
              $obj->labels[$stored_term->language] = array(
              'name' => $stored_term->name,
              'tid' => $stored_term->tid,
              );
             }
     }
  } 
  return $obj;
}
