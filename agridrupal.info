
; -----------------------------------------------------------------------------
;                             Introduction
; -----------------------------------------------------------------------------
; Well, this is it! An example Profiler include, showing you how to create your 
; own Install profiles easily. This file uses Drupal's info format, which is 
; very similar to PHP's own ini format, however you can use nested arrays in 
; Drupal's version, which comes in handy for lots of stuff, as you'll see below.
;
; "Wait, you said this would be easy, what is all this stuff?"
;
; There aren't really any mandatory things here, but if you don't put anything 
; in here, your install profile won't do much. :) Don't get overwhelmed, just
; go slow and make a few changes here and there till you get the hang of
; what's happening.
;
; I've tried to create examples for every piece of functionality below, but to 
; get a quick breakdown of what's currently available, here's a list of all the 
; possible items:
;
; - base
; - dependencies
; - nodes
; - terms
; - theme
; - users
; - variables
; -----------------------------------------------------------------------------

name = AgriDrupal
description = AgriDrupal is provided by AIMS (FAO)
core = 6.x
theme = garland

; -----------------------------------------------------------------------------
; You could specify this as a base Profiler include by using the following in
; your own Install profile's Profiler include. Then everything in this file
; would be merged with your Profiler include:
; -----------------------------------------------------------------------------
; base = profiler_example


; -----------------------------------------------------------------------------
;                                  Modules
; -----------------------------------------------------------------------------
; List the modules (core, contrib and/or features) to be enabled in the
; dependencies[] array. Any dependencies of the listed modules will be detected
; and enabled automatically.
; -----------------------------------------------------------------------------
dependencies[] = dlios

dependencies[] = agrovoc_api
dependencies[] = agrovocfield
dependencies[] = ahah_response
dependencies[] = amfphp
dependencies[] = auto_nodetitle
dependencies[] = backreference
dependencies[] = backup_migrate
dependencies[] = better_formats
dependencies[] = calendar
dependencies[] = captcha
dependencies[] = content
dependencies[] = fieldgroup
dependencies[] = nodereference
dependencies[] = optionwidgets
dependencies[] = text
dependencies[] = userreference
dependencies[] = computed_field
dependencies[] = conditional_fields
dependencies[] = contemplate
dependencies[] = content_profile
dependencies[] = content_taxonomy
dependencies[] = contexthelp
dependencies[] = ctools
dependencies[] = data
dependencies[] = date
dependencies[] = editview
dependencies[] = email
dependencies[] = evoc
dependencies[] = extlink
dependencies[] = faceted_search
dependencies[] = fckeditor
dependencies[] = features
dependencies[] = fe_block
dependencies[] = feeds
dependencies[] = feeds_tamper
dependencies[] = feeds_xpathparser
dependencies[] = fieldtool
dependencies[] = filterbynodetype
dependencies[] = flexifield
;dependencies[] = google_cse
;dependencies[] = graphmind
dependencies[] = i18n
dependencies[] = imce
dependencies[] = install_profile_api
dependencies[] = job_scheduler
dependencies[] = jquery_ui
dependencies[] = jquery_update
dependencies[] = jstools
dependencies[] = modalframe
dependencies[] = nice_menus
dependencies[] = node_import
dependencies[] = noderefcreate
dependencies[] = nodereference_views
dependencies[] = noderelationships
dependencies[] = nodesave_service
dependencies[] = notifications
dependencies[] = oai2ForCCK
dependencies[] = og
dependencies[] = og_forum
dependencies[] = pathauto
dependencies[] = pathfilter
dependencies[] = poormanscron
dependencies[] = rdf
dependencies[] = rdfcck
dependencies[] = rules
dependencies[] = services
dependencies[] = simplemenu
dependencies[] = sparql
dependencies[] = tac_lite
dependencies[] = taxonomy_csv
dependencies[] = taxonomy_manager
dependencies[] = token
dependencies[] = transformations
dependencies[] = transformations_drupal
dependencies[] = transformations_xml
dependencies[] = unique_field
dependencies[] = user_register_notify
dependencies[] = viewreference
dependencies[] = views
;dependencies[] = views_bonus
dependencies[] = views_bulk_operations
dependencies[] = views_cloud
dependencies[] = views_customfield
;dependencies[] = views_datasource
;dependencies[] = views_hacks
dependencies[] = vocabindex

; features
dependencies[] = dlios

; -----------------------------------------------------------------------------
;                                   Users
; -----------------------------------------------------------------------------
; One other thing to note here is that there's this (handy) quirk in Drupal's 
; install API in that you can bypass the configuration form during installation
; (where you'd normally set up user 1 credentials and the site name and such).
; To do this you need to specify credentials for user 1 and also specify the
; site name and site email variables (@see Variables below). 
; -----------------------------------------------------------------------------

users[superduper][uid]    = 1
users[superduper][name]   = admin
users[superduper][mail]   = admin@admin
users[superduper][roles]  = Admin,Publisher
users[superduper][status] = 1

users[siteadmin][uid]    = 2
users[siteadmin][name]   = siteadmin
users[siteadmin][mail]   = changeme@changeme
users[siteadmin][roles]  = Admin,Publisher
users[siteadmin][status] = 1

; -----------------------------------------------------------------------------
;                                  Variables
; -----------------------------------------------------------------------------
; These variables should be pretty self explanatory, as they are just an array
; of key value pairs for items in the variable table. Nested arrays also can 
; crop up here, since variables can be arrays.
;
; Also, make note of the site_name and site_mail variables below. These 
; variables need to be set if you want to bypass the Configuration form during 
; installation (@see Users for more information on how this is accomplished).
; -----------------------------------------------------------------------------
variables[site_name] = AgriDrupal
variables[site_mail] = testing@testing

variables[site_footer] = Provided by AIMS - FAO
variables[site_frontpage] = node/1
variables[theme_settings][toggle_node_info_page] = 0

; You may also want to set the clean url variable if you don't need clean urls. 
; Since we bypassed the configuration screen, it is enabled by default, so to 
; disable it you'll want to uncomment this line:
variables[clean_url] = 1

; -----------------------------------------------------------------------------
;                                    Nodes
; -----------------------------------------------------------------------------
nodes[front][type] = dlios
nodes[front][title] = AgriDrupal
nodes[front][uid] = 2
nodes[front][body] = <p>Donec vel ipsum elit. In rutrum, ante at fringilla pharetra, mi justo fermentum risus; eget commodo est mi lobortis augue. Proin fringilla malesuada semper. Ut velit sapien, condimentum vel faucibus ut, aliquam quis dui? Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vivamus vulputate fringilla enim, non convallis nibh lacinia at. Etiam fermentum, lectus ac sagittis volutpat, est augue tincidunt mi, et consectetur orci nisi sit amet sapien. Ut nisi nunc, molestie quis sollicitudin at, dignissim eget nisl? Sed vitae sem lectus. Curabitur viverra laoreet magna. Sed eget sapien tellus, eu tempor purus. Proin dignissim, lacus vitae venenatis porttitor, justo libero rhoncus felis, nec dictum nisl mi vitae nisi. Maecenas tincidunt eros at turpis sollicitudin nec viverra enim condimentum. Sed nec enim justo. In tristique risus dui; ac egestas mi. Proin at arcu sed enim eleifend ornare eu at arcu. Curabitur ullamcorper, ipsum eu imperdiet pharetra, mauris lorem hendrerit metus, in consectetur mauris est iaculis eros. Quisque euismod hendrerit metus ullamcorper tempus! Cras gravida ornare arcu vitae commodo. In hac habitasse platea turpis duis.</p>

; Information added by drupal.org packaging script on 2011-08-15
version = "6.x-2.0"
core = "6.x"
project = "profiler_example"
datestamp = "1313419319"

